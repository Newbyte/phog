# Phog

A greetd-compatible greeter for mobile devices like Purism's Librem 5 and
Pine64's PinePhone.

Targeting the [Phosh](https://gitlab.gnome.org/World/Phosh/phosh) (the PHOne
SHell) ecosystem, [Phog](https://gitlab.com/mobian1/phog) (the PHOne Greeter)
is meant to run on top of [Phoc](https://gitlab.gnome.org/World/Phosh/phoc)
(the PHOne Compositor). It provides a simple UI for logging with numeric-only
passwords, and relies on
[Squeekboard](https://gitlab.gnome.org/World/Phosh/squeekboard) for entering
more complex ones.

![](.assets/screenshots/phog-1.png) ![](.assets/screenshots/phog-2.png)

## License

Phog is licensed under the GPLv3+.

## Getting the source

```sh
git clone https://gitlab.com/mobian1/phog.git
cd phog
```

The [main](https://gitlab.com/mobian1/phog/-/tree/main) branch has the current
development version.

## Dependencies

The development versions of the following libraries are needed to build phog:

* libgcr-3
* libglib2.0 (>= v2.72)
* libgnome-desktop-3
* libgtk-3
* libgudev-1.0
* libjson-glib
* libhandy-1 (>= v1.1.90)
* libnm
* libpam0g
* libsystemd
* libupower-glib
* libwayland

On a Debian based system run:

```sh
sudo apt-get -y install build-essential
sudo apt-get -y build-dep ./.gitlab-ci
```

For the full list of dependencies, please refer to the
[.gitlab-ci/debian/control](https://gitlab.com/mobian1/phog/-/blob/main/.gitlab-ci/debian/control)
file.

## Building

We use the meson (and thereby Ninja) build system for phog. The quickest
way to get going is to do the following:

```sh
meson . _build
ninja -C _build
```

# Testing

To run the tests run

```sh
xvfb-run meson test -C _build
```

## Running

`phog` is not meant to be started by the user nor as a systemd service.
Instead, it must run as a child process of
[greetd](https://git.sr.ht/~kennylevinsen/greetd) which will handle the actual
user authentication and session startup.

In order to do so, `/etc/greet/config.toml` must contain something similar to
what follows:

```toml
[terminal]
# The VT to run the greeter on. Can be "next", "current" or a number
# designating the VT.
vt = 1

# The default session, also known as the greeter.
[default_session]

command = "phog"
# When using systemd, use the following command so the greeter's output is
# redirected to journald
# command = "systemd-cat --identifier=phog phog"

# The user to run the command as. The privileges this user must have depends
# on the greeter. A graphical greeter may for example require the user to be
# in the `video` group.
user = "greeter"
```

# Getting in Touch
* Issue tracker: https://gitlab.com/mobian1/phog/issues
