/*
 * Copyright (C) 2020 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */

#include "testlib.h"

G_BEGIN_DECLS

typedef struct _PhogTestFullShellFixture {
  GThread                  *comp_and_shell;
  GAsyncQueue              *queue;
  PhogTestCompositorState *state;
  GTestDBus                *bus;
  char                     *log_domains;
  char                     *tmpdir;
} PhogTestFullShellFixture;


typedef struct _PhogTestFullShellFixtureCfg {
  char *display;
  char *log_domains;
} PhogTestFullShellFixtureCfg;


PhogTestFullShellFixtureCfg *phog_test_full_shell_fixture_cfg_new (const char *display, const char *log_domains);
void phog_test_full_shell_fixture_cfg_dispose (PhogTestFullShellFixtureCfg *self);
void phog_test_full_shell_setup (PhogTestFullShellFixture *fixture, gconstpointer data);
void phog_test_full_shell_teardown (PhogTestFullShellFixture *fixture, gconstpointer unused);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (PhogTestFullShellFixtureCfg, phog_test_full_shell_fixture_cfg_dispose)

G_END_DECLS
