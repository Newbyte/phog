/*
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define PHOG_TYPE_WWAN (phog_wwan_get_type())
G_DECLARE_INTERFACE (PhogWWan, phog_wwan, PHOG, WWAN, GObject)

/**
 * PhogWWanInterface
 * @parent_iface: The parent interface
 * @get_signal_quality: Get the current signal quality
 * @get_access_tec: Get the current access technology (2G, 3G, ...)
 * @is_unlocked: whether the SIM in the modem is locked
 * @has_sim: Whether there's a sim in the modem
 * @is_present: whether a modem is present at all
 * @is_enabled: whether a modem is enabled
 * @get_operator: Get the current network operator name
 *
 * Provides an interface for interacting with a modem
*/

struct _PhogWWanInterface
{
  GTypeInterface parent_iface;

  guint         (*get_signal_quality) (PhogWWan *self);
  const char*   (*get_access_tec)     (PhogWWan *self);
  gboolean      (*is_unlocked)        (PhogWWan *self);
  gboolean      (*has_sim)            (PhogWWan *self);
  gboolean      (*is_present)         (PhogWWan *self);
  gboolean      (*is_enabled)         (PhogWWan *self);
  const char*   (*get_operator)       (PhogWWan *self);
};

guint         phog_wwan_get_signal_quality (PhogWWan* self);
const char*   phog_wwan_get_access_tec     (PhogWWan* self);
gboolean      phog_wwan_is_unlocked        (PhogWWan* self);
gboolean      phog_wwan_has_sim            (PhogWWan* self);
gboolean      phog_wwan_is_present         (PhogWWan* self);
gboolean      phog_wwan_is_enabled         (PhogWWan *self);
void          phog_wwan_set_enabled        (PhogWWan *self, gboolean enabled);
const char   *phog_wwan_get_operator       (PhogWWan *self);
