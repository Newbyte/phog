/*
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#pragma once

#include <gtk/gtk.h>
#include "layersurface.h"

#define PHOG_TYPE_LOCKSHIELD (phog_lockshield_get_type())

G_DECLARE_FINAL_TYPE (PhogLockshield, phog_lockshield, PHOG, LOCKSHIELD, PhogLayerSurface)

GtkWidget * phog_lockshield_new (gpointer layer_shell,
                                  gpointer wl_output);
