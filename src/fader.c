/*
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */

#define G_LOG_DOMAIN "phog-fader"

#include "phog-config.h"
#include "fader.h"
#include "shell.h"

#include <handy.h>

/**
 * SECTION:fader
 * @short_description: A fader
 * @Title: PhogFader
 *
 * A fullsreen surface that fades in or out.
 */

#define PHOG_FADER_DEFAULT_STYLE_CLASS "phog-fader-default-fade"

enum {
  PROP_0,
  PROP_MONITOR,
  PROP_STYLE_CLASS,
  PROP_LAST_PROP,
};
static GParamSpec *props[PROP_LAST_PROP];

typedef struct _PhogFader
{
  PhogLayerSurface           parent;
  PhogMonitor               *monitor;
  char                       *style_class;
} PhogFader;
G_DEFINE_TYPE (PhogFader, phog_fader, PHOG_TYPE_LAYER_SURFACE)


static void
phog_fader_set_property (GObject      *obj,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  PhogFader *self = PHOG_FADER (obj);

  switch (prop_id) {
  case PROP_MONITOR:
    /* construct only */
    g_set_object (&self->monitor, g_value_get_object (value));
    break;
  case PROP_STYLE_CLASS:
    g_free (self->style_class);
    self->style_class = g_value_dup_string (value);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (obj, prop_id, pspec);
    break;
  }
}


static void
phog_fader_get_property (GObject    *obj,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  PhogFader *self = PHOG_FADER (obj);

  switch (prop_id) {
  case PROP_MONITOR:
    g_value_set_object (value, self->monitor);
    break;
  case PROP_STYLE_CLASS:
    g_value_set_string (value, self->style_class);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (obj, prop_id, pspec);
    break;
  }
}


static void
phog_fader_show (GtkWidget *widget)
{
  PhogFader *self = PHOG_FADER (widget);
  gboolean enable_animations;
  GtkStyleContext *context;

  enable_animations = hdy_get_enable_animations (widget);

  if (enable_animations) {
    const char *style_class;

    style_class = self->style_class ?: PHOG_FADER_DEFAULT_STYLE_CLASS;
    context = gtk_widget_get_style_context (widget);
    gtk_style_context_add_class (context, style_class);
  }

  GTK_WIDGET_CLASS (phog_fader_parent_class)->show (widget);
}


static void
phog_fader_dispose (GObject *object)
{
  PhogFader *self = PHOG_FADER (object);

  g_clear_object (&self->monitor);
  g_clear_pointer (&self->style_class, g_free);

  G_OBJECT_CLASS (phog_fader_parent_class)->dispose (object);
}


static void
phog_fader_constructed (GObject *object)
{
  PhogFader *self = PHOG_FADER (object);
  PhogWayland *wl = phog_wayland_get_default ();

  if (self->monitor == NULL)
    self->monitor = g_object_ref (phog_shell_get_primary_monitor (phog_shell_get_default ()));

  g_object_set (PHOG_LAYER_SURFACE (self),
                "layer-shell", phog_wayland_get_zwlr_layer_shell_v1 (wl),
                "wl-output", phog_monitor_get_wl_output (self->monitor),
                "anchor", ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP |
                ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM |
                ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT |
                ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT,
                "layer", ZWLR_LAYER_SHELL_V1_LAYER_OVERLAY,
                "kbd-interactivity", FALSE,
                "exclusive-zone", -1,
                "namespace", "phog fader",
                NULL);

  G_OBJECT_CLASS (phog_fader_parent_class)->constructed (object);
}


static void
phog_fader_class_init (PhogFaderClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->get_property = phog_fader_get_property;
  object_class->set_property = phog_fader_set_property;
  object_class->constructed = phog_fader_constructed;
  object_class->dispose = phog_fader_dispose;
  widget_class->show = phog_fader_show;

  props[PROP_MONITOR] = g_param_spec_object ("monitor",
                                             "",
                                             "",
                                             PHOG_TYPE_MONITOR,
                                             G_PARAM_CONSTRUCT_ONLY |
                                             G_PARAM_READWRITE |
                                             G_PARAM_STATIC_STRINGS);

  /**
   * PhogFader:style-class
   *
   * The CSS style class used for the animation
   */
  props[PROP_STYLE_CLASS] = g_param_spec_string ("style-class",
                                                 "",
                                                 "",
                                                 NULL,
                                                 G_PARAM_CONSTRUCT_ONLY |
                                                 G_PARAM_READWRITE |
                                                 G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, PROP_LAST_PROP, props);
}


static void
phog_fader_init (PhogFader *self)
{
  self->style_class = g_strdup (PHOG_FADER_DEFAULT_STYLE_CLASS);
}


PhogFader *
phog_fader_new (PhogMonitor *monitor)
{
  return g_object_new (PHOG_TYPE_FADER, "monitor", monitor, NULL);
}
