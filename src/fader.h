/*
 * Copyright (C) 2019 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#pragma once

#include "monitor/monitor.h"

#include <layersurface.h>

G_BEGIN_DECLS

#define PHOG_TYPE_FADER (phog_fader_get_type())

G_DECLARE_FINAL_TYPE (PhogFader, phog_fader, PHOG, FADER, PhogLayerSurface)

PhogFader *phog_fader_new (PhogMonitor *monitor);

G_END_DECLS
