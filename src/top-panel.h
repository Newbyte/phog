/*
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "drag-surface.h"

#define PHOG_TYPE_TOP_PANEL            (phog_top_panel_get_type ())

G_DECLARE_FINAL_TYPE (PhogTopPanel, phog_top_panel, PHOG, TOP_PANEL, PhogDragSurface)

#define PHOG_TOP_PANEL_HEIGHT 32

/**
 * PhogTopPanelState:
 * @PHOG_TOP_PANEL_STATE_FOLDED: Only top-bar is visible
 * @PHOG_TOP_PANEL_STATE_UNFOLDED: Settings menu is unfolded
 */
typedef enum {
  PHOG_TOP_PANEL_STATE_FOLDED,
  PHOG_TOP_PANEL_STATE_UNFOLDED,
} PhogTopPanelState;

GtkWidget         *phog_top_panel_new (struct zwlr_layer_shell_v1          *layer_shell,
                                        struct zphoc_layer_shell_effects_v1 *layer_shell_effects,
                                        struct wl_output                    *wl_output,
                                        guint32                              layer,
                                        int                                  height);
void               phog_top_panel_toggle_fold (PhogTopPanel *self);
void               phog_top_panel_fold (PhogTopPanel *self);
void               phog_top_panel_unfold (PhogTopPanel *self);
PhogTopPanelState phog_top_panel_get_state (PhogTopPanel *self);
