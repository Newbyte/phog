/*
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>
/* TODO: We use the enum constants from here, use glib-mkenums */
#include "wlr-layer-shell-unstable-v1-client-protocol.h"

G_BEGIN_DECLS

#define PHOG_TYPE_LAYER_SURFACE                 (phog_layer_surface_get_type ())

G_DECLARE_DERIVABLE_TYPE (PhogLayerSurface, phog_layer_surface, PHOG, LAYER_SURFACE, GtkWindow)

/**
 * PhogLayerSurfaceClass
 * @parent_class: The parent class
 * @configured: invoked when layer surface is configured
 */
struct _PhogLayerSurfaceClass
{
  GtkWindowClass parent_class;

  /* Signals
   */
  void (*configured)   (PhogLayerSurface    *self);
};

GtkWidget *phog_layer_surface_new (gpointer layer_shell,
                                    gpointer wl_output);
struct     zwlr_layer_surface_v1 *phog_layer_surface_get_layer_surface(PhogLayerSurface *self);
struct     wl_surface            *phog_layer_surface_get_wl_surface(PhogLayerSurface *self);
void                              phog_layer_surface_set_size(PhogLayerSurface *self,
                                                               int width,
                                                               int height);
void                              phog_layer_surface_set_margins(PhogLayerSurface *self,
                                                                  int top,
                                                                  int right,
                                                                  int bottom,
                                                                  int left);
void                              phog_layer_surface_set_exclusive_zone(PhogLayerSurface *self,
                                                                         int zone);
void                              phog_layer_surface_set_kbd_interactivity(PhogLayerSurface *self,
                                                                            gboolean interactivity);
void                              phog_layer_surface_set_layer (PhogLayerSurface *self,
                                                                 guint32            layer);
void                              phog_layer_surface_wl_surface_commit (PhogLayerSurface *self);
void                              phog_layer_surface_get_margins       (PhogLayerSurface *self,
                                                                         int               *top,
                                                                         int               *right,
                                                                         int               *bottom,
                                                                         int               *left);
int                               phog_layer_surface_get_configured_width  (PhogLayerSurface *self);
int                               phog_layer_surface_get_configured_height (PhogLayerSurface *self);

G_END_DECLS
