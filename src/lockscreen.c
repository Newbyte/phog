/*
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Guido Günther <agx@sigxcpu.org>
 */

#define G_LOG_DOMAIN "phog-lockscreen"

#include "phog-config.h"

#include "greetd.h"
#include "greetd-session.h"
#include "keypad.h"
#include "lockscreen.h"
#include "osk-button.h"
#include "osk-manager.h"
#include "shell.h"
#include "util.h"

#include <string.h>
#include <math.h>
#include <time.h>

#define GCR_API_SUBJECT_TO_CHANGE
#include <gcr/gcr.h>
#include <glib/gi18n.h>
#include <handy.h>

#define LOCKSCREEN_IDLE_SECONDS 5
#define LOCKSCREEN_LARGE_DATE_AND_TIME_CLASS "p-large"
#define LOCKSCREEN_SMALL_DATE_AND_TIME_CLASS "p-small"

#define LOGO_FILE_PATH "/etc/phog/logo.png"

/**
 * SECTION:lockscreen
 * @short_description: The main lock screen
 * @Title: PhogLockscreen
 *
 * The lock screen featuring the clock
 * and unlock keypad.
 *
 * # CSS nodes
 *
 * #PhogLockscreen has a CSS name with the name phog-lockscreen.
 */


typedef enum {
  POS_OVERVIEW = 0,
  POS_UNLOCK   = 1,
} PhogLocksreenPos;

enum {
  LOCKSCREEN_UNLOCK,
  WAKEUP_OUTPUT,
  N_SIGNALS
};
static guint signals[N_SIGNALS] = { 0 };

typedef struct _PhogLockscreen
{
  PhogLayerSurface parent;
} PhogLockscreen;


typedef struct {
  GtkWidget         *carousel;
  PhogGreetd        *greetd;

  /* info page */
  GtkWidget         *box_info;
  GtkWidget         *box_users;
  GtkWidget         *row_sessions;
  GtkWidget         *logo;
  gchar             *current_user;

  /* unlock page */
  GtkWidget         *box_unlock;
  GtkWidget         *keypad_revealer;
  GtkWidget         *keypad;
  GtkWidget         *entry_pin;
  GtkGesture        *long_press_del_gesture;
  GtkWidget         *lbl_unlock_status;
  GtkWidget         *btn_submit;
  GtkWidget         *btn_emergency;
  guint              idle_timer;
  gint64             last_input;
} PhogLockscreenPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (PhogLockscreen, phog_lockscreen, PHOG_TYPE_LAYER_SURFACE)

static void
clear_input (PhogLockscreen *self, gboolean clear_all)
{
  PhogLockscreenPrivate *priv = phog_lockscreen_get_instance_private (self);

  if (clear_all) {
    gtk_label_set_label (GTK_LABEL (priv->lbl_unlock_status), _("Enter Passcode"));
    gtk_editable_delete_text (GTK_EDITABLE (priv->entry_pin), 0, -1);
  } else {
    g_signal_emit_by_name (priv->entry_pin, "backspace", NULL);
  }
}

static void
show_info_page (PhogLockscreen *self)
{
  PhogLockscreenPrivate *priv = phog_lockscreen_get_instance_private (self);

  if (hdy_carousel_get_position (HDY_CAROUSEL (priv->carousel)) <= 0)
    return;

  hdy_carousel_scroll_to (HDY_CAROUSEL (priv->carousel), priv->box_info);
}


static gboolean
keypad_check_idle (PhogLockscreen *self)
{
  PhogLockscreenPrivate *priv = phog_lockscreen_get_instance_private (self);
  gint64 now = g_get_monotonic_time ();

  g_assert (PHOG_IS_LOCKSCREEN (self));
  if (now - priv->last_input > LOCKSCREEN_IDLE_SECONDS * 1000 * 1000) {
    show_info_page (self);
    priv->idle_timer = 0;
    return FALSE;
  }
  return TRUE;
}


static void
show_unlock_page (PhogLockscreen *self)
{
  PhogLockscreenPrivate *priv = phog_lockscreen_get_instance_private (self);

  if (hdy_carousel_get_position (HDY_CAROUSEL (priv->carousel)) >= POS_UNLOCK)
    return;

  hdy_carousel_scroll_to (HDY_CAROUSEL (priv->carousel), priv->box_unlock);

  /* skip signal on init */
  if (signals[WAKEUP_OUTPUT])
    g_signal_emit (self, signals[WAKEUP_OUTPUT], 0);
}


static gboolean
finish_shake_label (PhogLockscreen *self)
{
  PhogLockscreenPrivate *priv = phog_lockscreen_get_instance_private (self);
  g_autoptr(GError) error = NULL;

  clear_input (self, TRUE);
  gtk_widget_set_sensitive (GTK_WIDGET (self), TRUE);

  /* greetd automatically destroys the current session on error, so we must re-create it */
  if (!phog_greetd_create_session(priv->greetd, priv->current_user, &error))
    show_info_page (self);

  return FALSE;
}


static gboolean
shake_label (GtkWidget     *widget,
             GdkFrameClock *frame_clock,
             gpointer       data)
{
  PhogLockscreen *self = PHOG_LOCKSCREEN (widget);
  PhogLockscreenPrivate *priv = phog_lockscreen_get_instance_private (self);
  gint64 start_time = g_variant_get_int64 (data);
  gint64 end_time = start_time + 1000 * 300;
  gint64 now = gdk_frame_clock_get_frame_time (frame_clock);

  float t = (float) (now - start_time) / (float) (end_time - start_time);
  float pos = sin (t * 10) * 0.05 + 0.5;

  if (now > end_time) {
    /* Stop the animation only when we would step over the idle position (0.5) */
    if ((gtk_entry_get_alignment (GTK_ENTRY (priv->entry_pin)) > 0.5 && pos < 0.5) || pos > 0.5) {
      gtk_entry_set_alignment (GTK_ENTRY (priv->entry_pin), 0.5);
      g_timeout_add (400, (GSourceFunc) finish_shake_label, self);
      return FALSE;
    }
  }

  gtk_entry_set_alignment (GTK_ENTRY (priv->entry_pin), pos);
  return TRUE;
}

static void
focus_pin_entry (PhogLockscreen *self, gboolean enable_osk)
{
  PhogLockscreenPrivate *priv = phog_lockscreen_get_instance_private (self);

  if (enable_osk) {
    /* restore default OSK behavior */
    g_object_set (priv->entry_pin, "im-module", NULL, NULL);
  }

  gtk_entry_grab_focus_without_selecting (GTK_ENTRY (priv->entry_pin));
}


static void
delete_button_clicked_cb (PhogLockscreen *self,
                          GtkWidget       *widget)
{
  g_return_if_fail (PHOG_IS_LOCKSCREEN (self));

  clear_input (self, FALSE);
}

static void
osk_button_clicked_cb (PhogLockscreen *self,
                       GtkWidget       *widget)
{
  PhogLockscreenPrivate *priv;

  g_assert (PHOG_IS_LOCKSCREEN (self));
  priv = phog_lockscreen_get_instance_private (self);

  priv->last_input = g_get_monotonic_time ();

  focus_pin_entry (self, TRUE);
}


static void
on_osk_visibility_changed (PhogLockscreen *self,
                           GParamSpec      *pspec,
                           PhogOskManager *osk)
{
  PhogLockscreenPrivate *priv;

  g_assert (PHOG_IS_LOCKSCREEN (self));
  priv = phog_lockscreen_get_instance_private (self);

  if (!phog_osk_manager_get_visible (osk)) {
    g_object_set (priv->entry_pin, "im-module", "gtk-im-context-none", NULL);
  }
}


static void
long_press_del_cb (PhogLockscreen *self,
                   double           x,
                   double           y,
                   GtkGesture      *gesture)
{
  g_return_if_fail (PHOG_IS_LOCKSCREEN (self));
  g_debug ("Long press on delete button");
  clear_input (self, TRUE);
}


static void
input_changed_cb (PhogLockscreen *self)
{
  PhogLockscreenPrivate *priv;
  guint16 length;

  g_assert (PHOG_IS_LOCKSCREEN (self));
  priv = phog_lockscreen_get_instance_private (self);
  priv->last_input = g_get_monotonic_time ();

  length = gtk_entry_get_text_length (GTK_ENTRY (priv->entry_pin));

  gtk_widget_set_sensitive (priv->btn_submit, length != 0);
}


static void
submit_cb (PhogLockscreen *self)
{
  PhogLockscreenPrivate *priv;
  const char *input;
  guint16 length;
  gint session_id;
  g_autoptr(GError) error = NULL;

  g_assert (PHOG_IS_LOCKSCREEN (self));

  priv = phog_lockscreen_get_instance_private (self);
  session_id = hdy_combo_row_get_selected_index (HDY_COMBO_ROW (priv->row_sessions));
  priv->last_input = g_get_monotonic_time ();

  length = gtk_entry_get_text_length (GTK_ENTRY (priv->entry_pin));
  if (length == 0) {
    return;
  }

  input = gtk_entry_get_text (GTK_ENTRY (priv->entry_pin));

  gtk_label_set_label (GTK_LABEL (priv->lbl_unlock_status), _("Checking…"));
  gtk_widget_set_sensitive (GTK_WIDGET (self), FALSE);

  if (!phog_greetd_authenticate(priv->greetd, input, &error)) {
    GdkFrameClock *clock;
    gint64 now;
    g_warning("Error while authenticating: %s", error->message);
    /* give visual feedback on error */
    clock = gtk_widget_get_frame_clock (priv->entry_pin);
    now = gdk_frame_clock_get_frame_time (clock);
    gtk_widget_add_tick_callback (GTK_WIDGET (self),
                                  shake_label,
                                  g_variant_ref_sink (g_variant_new_int64 (now)),
                                  (GDestroyNotify) g_variant_unref);
    phog_keypad_distribute (PHOG_KEYPAD (priv->keypad));
    return;
  }

  /* Let's start the session separately, so we don't give the user the wrong feedback */
  if (!phog_greetd_start_session(priv->greetd, session_id, &error)) {
    g_warning("Error while starting session: %s", error->message);
    show_info_page (self);
    return;
  }

  g_message("All good, quitting...");
  gtk_main_quit();
}


static gboolean
key_press_event_cb (PhogLockscreen *self, GdkEventKey *event, gpointer data)
{
  PhogLockscreenPrivate *priv;
  gboolean handled = FALSE;
  gboolean on_unlock_page;
  double position;

  g_assert (PHOG_IS_LOCKSCREEN (self));
  priv = phog_lockscreen_get_instance_private (self);

  position = hdy_carousel_get_position (HDY_CAROUSEL (priv->carousel));
  /* Round to nearest page so we already accept keyboard input before animation ends */
  on_unlock_page = (int)round(position) == POS_UNLOCK;

  if (gtk_entry_im_context_filter_keypress (GTK_ENTRY (priv->entry_pin), event)) {
    show_unlock_page (self);
    handled = TRUE;
  } else {
    switch (event->keyval) {
    case GDK_KEY_space:
      show_unlock_page (self);
      handled = TRUE;
      break;
    case GDK_KEY_Escape:
      clear_input (self, TRUE);
      show_info_page (self);
      handled = TRUE;
      break;
    case GDK_KEY_Delete:
    case GDK_KEY_KP_Delete:
    case GDK_KEY_BackSpace:
      if (on_unlock_page == TRUE) {
        clear_input (self, FALSE);
        handled = TRUE;
      }
      break;
    case GDK_KEY_Return:
    case GDK_KEY_ISO_Enter:
    case GDK_KEY_KP_Enter:
      if (on_unlock_page == TRUE) {
        submit_cb (self);
        handled = TRUE;
      }
      break;
    default:
      /* nothing to do */
      break;
    }
  }

  priv->last_input = g_get_monotonic_time ();
  return handled;
}


static void
carousel_position_notified_cb (PhogLockscreen *self,
                               GParamSpec      *pspec,
                               HdyCarousel     *carousel)
{
  PhogLockscreenPrivate *priv = phog_lockscreen_get_instance_private (self);
  double position;

  position = hdy_carousel_get_position (HDY_CAROUSEL (priv->carousel));

  if (position <= POS_OVERVIEW || position >= POS_UNLOCK)
    return;

  if (priv->idle_timer) {
    g_source_remove (priv->idle_timer);
    priv->idle_timer = 0;
  }
}

static void
carousel_page_changed_cb (PhogLockscreen *self,
                          guint            index,
                          HdyCarousel     *carousel)
{
  PhogLockscreenPrivate *priv = phog_lockscreen_get_instance_private (self);
  PhogShell *shell = phog_shell_get_default ();
  PhogOskManager *osk_manager = phog_shell_get_osk_manager (shell);
  gboolean osk_visible = phog_osk_manager_get_visible (osk_manager);
  g_autoptr(GError) error = NULL;

  if (index == POS_OVERVIEW) {
    clear_input (self, TRUE);
    gtk_widget_set_sensitive (priv->entry_pin, FALSE);
    phog_greetd_cancel_session(priv->greetd, &error);
  }

  if (index == POS_UNLOCK) {
    if (!phog_greetd_create_session(priv->greetd, priv->current_user, &error)) {
      show_info_page (self);
      return;
    }

    gtk_widget_set_sensitive (priv->entry_pin, TRUE);

    focus_pin_entry (self, osk_visible);

    if (!priv->idle_timer) {
      priv->last_input = g_get_monotonic_time ();
      priv->idle_timer = g_timeout_add_seconds (LOCKSCREEN_IDLE_SECONDS,
                                                (GSourceFunc) keypad_check_idle,
                                                self);
    }
  }
}

static void
on_row_activated (PhogLockscreen *self, GtkWidget *row)
{
  PhogLockscreenPrivate *priv = phog_lockscreen_get_instance_private (self);
  g_autoptr(GError) error = NULL;
  const gchar *login = NULL;

  g_return_if_fail(HDY_IS_ACTION_ROW (row));

  login = hdy_action_row_get_subtitle (HDY_ACTION_ROW (row));
  if (!login || !strlen (login))
    login = hdy_preferences_row_get_title (HDY_PREFERENCES_ROW (row));

  g_return_if_fail(login);

  if (!g_str_equal (priv->current_user, login)) {
    g_free (priv->current_user);
    priv->current_user = g_strdup (login);
  }

  show_unlock_page (self);
}

static void
lockscreen_fill_users_list (void *key, void *value, void *data)
{
  PhogLockscreen *self = data;
  PhogLockscreenPrivate *priv = phog_lockscreen_get_instance_private (self);
  GtkWidget *row;
  gchar *login = key, *real_name = value;

  row = hdy_action_row_new ();

  if (!priv->current_user)
    priv->current_user = g_strdup (login);

  if (real_name && (strlen (real_name) > 0) && !g_str_equal(login, real_name)) {
    hdy_preferences_row_set_title (HDY_PREFERENCES_ROW (row), real_name);
    hdy_action_row_set_subtitle (HDY_ACTION_ROW (row), login);
  } else {
    hdy_preferences_row_set_title (HDY_PREFERENCES_ROW (row), login);
  }

  gtk_list_box_row_set_activatable (GTK_LIST_BOX_ROW (row), TRUE);

  gtk_list_box_insert (GTK_LIST_BOX (priv->box_users), row, -1);
  gtk_widget_show (row);
}

static gchar *
lockscreen_name_for_session (gpointer object, gpointer data)
{
  PhogGreetdSession *session = PHOG_GREETD_SESSION (object);
  return g_strdup (phog_greetd_session_get_name (session));
}

static void
phog_lockscreen_constructed (GObject *object)
{
  PhogLockscreen *self = PHOG_LOCKSCREEN (object);
  PhogLockscreenPrivate *priv = phog_lockscreen_get_instance_private (self);
  PhogShell *shell;
  guint session_idx;

  G_OBJECT_CLASS (phog_lockscreen_parent_class)->constructed (object);

  /* window properties */
  gtk_window_set_title (GTK_WINDOW (self), "phog lockscreen");
  gtk_window_set_decorated (GTK_WINDOW (self), FALSE);
  gtk_widget_realize (GTK_WIDGET (self));

  gtk_widget_add_events (GTK_WIDGET (self), GDK_KEY_PRESS_MASK);
  g_signal_connect (G_OBJECT (self),
                    "key_press_event",
                    G_CALLBACK (key_press_event_cb),
                    NULL);

  shell = phog_shell_get_default ();
  g_object_bind_property (phog_shell_get_osk_manager (shell), "visible",
                          priv->keypad_revealer, "reveal-child",
                          G_BINDING_SYNC_CREATE | G_BINDING_INVERT_BOOLEAN);
  g_signal_connect_object (phog_shell_get_osk_manager (shell), "notify::visible",
                           G_CALLBACK (on_osk_visibility_changed), self,
                           G_CONNECT_SWAPPED);

  g_signal_connect_object (G_OBJECT (priv->box_users),
                           "row-activated",
                           G_CALLBACK (on_row_activated),
                           self,
                           G_CONNECT_SWAPPED);

  priv->greetd = PHOG_GREETD (phog_greetd_new ());
  g_hash_table_foreach(phog_greetd_get_available_users (priv->greetd),
                       lockscreen_fill_users_list,
                       self);

  hdy_combo_row_bind_name_model (HDY_COMBO_ROW (priv->row_sessions),
                                 G_LIST_MODEL (phog_greetd_get_available_sessions (priv->greetd)),
                                 lockscreen_name_for_session,
                                 NULL,
                                 NULL);

  if (g_list_model_get_n_items (hdy_combo_row_get_model (HDY_COMBO_ROW (priv->row_sessions))) <= 1)
    gtk_widget_set_visible (priv->row_sessions, FALSE);

  session_idx = phog_greetd_get_last_session_idx (priv->greetd);
  if (session_idx != G_MAXUINT)
    hdy_combo_row_set_selected_index (HDY_COMBO_ROW (priv->row_sessions), session_idx);

  if (g_file_test (LOGO_FILE_PATH, G_FILE_TEST_EXISTS))
    gtk_image_set_from_file(GTK_IMAGE (priv->logo), LOGO_FILE_PATH);
}


static void
phog_lockscreen_dispose (GObject *object)
{
  PhogLockscreen *self = PHOG_LOCKSCREEN (object);
  PhogLockscreenPrivate *priv = phog_lockscreen_get_instance_private (self);

  g_clear_object (&priv->greetd);
  g_free (priv->current_user);
  g_clear_handle_id (&priv->idle_timer, g_source_remove);

  G_OBJECT_CLASS (phog_lockscreen_parent_class)->dispose (object);
}


static void
phog_lockscreen_class_init (PhogLockscreenClass *klass)
{
  GObjectClass *object_class = (GObjectClass *)klass;
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->constructed = phog_lockscreen_constructed;
  object_class->dispose = phog_lockscreen_dispose;

  signals[LOCKSCREEN_UNLOCK] = g_signal_new ("lockscreen-unlock",
                                             G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST, 0, NULL, NULL,
                                             NULL, G_TYPE_NONE, 0);
  /**
   * PhogLockscreen::wakeup-output
   * @self: The #PhogLockscreen emitting this signal
   *
   * Emitted when the output showing the lock screen should be woken
   * up.
   */
  signals[WAKEUP_OUTPUT] = g_signal_new ("wakeup-output",
                                         G_TYPE_FROM_CLASS (klass), G_SIGNAL_RUN_LAST, 0, NULL, NULL,
                                         NULL, G_TYPE_NONE, 0);

  g_type_ensure (PHOG_TYPE_KEYPAD);
  g_type_ensure (PHOG_TYPE_OSK_BUTTON);
  g_type_ensure (GCR_TYPE_SECURE_ENTRY_BUFFER);
  gtk_widget_class_set_css_name (widget_class, "phog-lockscreen");
  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/org/mobian/phog/ui/lockscreen.ui");
  gtk_widget_class_bind_template_child_private (widget_class, PhogLockscreen, carousel);
  gtk_widget_class_bind_template_callback_full (widget_class,
                                                "carousel_position_notified_cb",
                                                G_CALLBACK (carousel_position_notified_cb));
  gtk_widget_class_bind_template_callback_full (widget_class,
                                                "carousel_page_changed_cb",
                                                G_CALLBACK (carousel_page_changed_cb));

  /* unlock page */
  gtk_widget_class_bind_template_child_private (widget_class, PhogLockscreen, box_unlock);
  gtk_widget_class_bind_template_child_private (widget_class, PhogLockscreen, keypad);
  gtk_widget_class_bind_template_child_private (widget_class, PhogLockscreen, keypad_revealer);
  gtk_widget_class_bind_template_child_private (widget_class, PhogLockscreen, entry_pin);
  gtk_widget_class_bind_template_child_private (widget_class, PhogLockscreen, lbl_unlock_status);
  gtk_widget_class_bind_template_child_private (widget_class, PhogLockscreen, long_press_del_gesture);
  gtk_widget_class_bind_template_child_private (widget_class, PhogLockscreen, btn_submit);
  gtk_widget_class_bind_template_child_private (widget_class, PhogLockscreen, btn_emergency);

  gtk_widget_class_bind_template_callback (widget_class, long_press_del_cb);
  gtk_widget_class_bind_template_callback (widget_class, delete_button_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, osk_button_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, submit_cb);
  gtk_widget_class_bind_template_callback (widget_class, input_changed_cb);

  /* info page */
  gtk_widget_class_bind_template_child_private (widget_class, PhogLockscreen, box_info);
  gtk_widget_class_bind_template_child_private (widget_class, PhogLockscreen, box_users);
  gtk_widget_class_bind_template_child_private (widget_class, PhogLockscreen, row_sessions);
  gtk_widget_class_bind_template_child_private (widget_class, PhogLockscreen, logo);

  gtk_widget_class_bind_template_callback (widget_class, show_unlock_page);
}


static void
phog_lockscreen_init (PhogLockscreen *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}


GtkWidget *
phog_lockscreen_new (gpointer layer_shell,
                      gpointer wl_output)
{
  return g_object_new (PHOG_TYPE_LOCKSCREEN,
                       "layer-shell", layer_shell,
                       "wl-output", wl_output,
                       "anchor", ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP |
                                 ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM |
                                 ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT |
                                 ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT,
                       "layer", ZWLR_LAYER_SHELL_V1_LAYER_OVERLAY,
                       "kbd-interactivity", TRUE,
                       "exclusive-zone", -1,
                       "namespace", "phog lockscreen",
                       NULL);
}

/**
 * phog_lockscreen_get_page
 * @self: The #PhogLockscreen
 *
 * Returns: The #PhogLockscreenPage that is currently shown
 */
PhogLockscreenPage
phog_lockscreen_get_page (PhogLockscreen *self)
{
  PhogLockscreenPrivate *priv;
  gdouble position;

  g_return_val_if_fail (PHOG_IS_LOCKSCREEN (self), PHOG_LOCKSCREEN_PAGE_DEFAULT);
  priv = phog_lockscreen_get_instance_private (self);
  position = hdy_carousel_get_position (HDY_CAROUSEL (priv->carousel));

  if (position <= 0)
    return PHOG_LOCKSCREEN_PAGE_DEFAULT;
  else
    return PHOG_LOCKSCREEN_PAGE_UNLOCK;
}

/*
 * phog_lockscreen_set_page
 * @self: The #PhogLockscreen
 * PhogLockscreenPage: the page to scroll to
 *
 * Scrolls to a specific page in the carousel. The state of the deck isn't changed.
 */
void
phog_lockscreen_set_page (PhogLockscreen *self, PhogLockscreenPage page)
{
  GtkWidget *scroll_to;
  PhogLockscreenPrivate *priv;

  g_return_if_fail (PHOG_IS_LOCKSCREEN (self));
  priv = phog_lockscreen_get_instance_private (self);

  scroll_to = (page == PHOG_LOCKSCREEN_PAGE_UNLOCK) ? priv->box_unlock : priv->box_info;

  hdy_carousel_scroll_to (HDY_CAROUSEL (priv->carousel), scroll_to);
}
